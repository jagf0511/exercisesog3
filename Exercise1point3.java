/* Programe create by
    Julian Andres Guisao Fernandez
    U.P.B
*/

    class Exercise1point3 {
        public static void main(String args[]){
            int Radius, RadiusP;
            double Pi, Area, Perimeter; // Variable assignment
            Radius=4;
            Pi=3.1416;
            RadiusP=Radius*Radius; // RadiusP is radius of power 2
            Area=Pi*RadiusP;
            Perimeter=2*Pi*Radius;
            System.out.println("The area and perimeter of the circle is " + Area + " ," + Perimeter);
        }
    }