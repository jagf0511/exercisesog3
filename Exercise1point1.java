/* Programe created by
    Julián Andrés Guisao Fernández
    U.P.B
*/

class Exercise1point1 {
    public static void main(String args[]) {
        int Date1, Date2, Addition, Subtraction, Multiplication, Division; //Variable assignment
        Date1=1000;
        Date2=10;
        Addition=Date1+Date2; //Calculation of results
        Subtraction=Date1-Date2;
        Multiplication=Date1*Date2;
        Division=Date1/Date2;
        System.out.println("The result of your operations is "+ Addition + "," + Subtraction + "," + Multiplication + "," + Division);
    }

}